using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Camera CentralCamera;
    public Camera TpsCamera;
    public Camera FrontCamera;
    //public Camera ZonaCamera;
    public Camera MouffleCamera;


    // Start is called before the first frame update
    void Start()
    {
        CentralCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        TpsCamera = GameObject.Find("Tps").GetComponent<Camera>();
        FrontCamera = GameObject.Find("Front").GetComponent<Camera>();
        //ZonaCamera = GameObject.Find("ZonaCamera").GetComponent<Camera>();
        MouffleCamera = GameObject.Find("MouffleCamera").GetComponent<Camera>();
        //camera par defaut
        ShowCentralCameraView();
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Keypad1)){
            ShowCentralCameraView();
        }

        if (Input.GetKeyDown(KeyCode.Keypad2)){
            ShowTpsCameraView();
        }

        if (Input.GetKeyDown(KeyCode.Keypad3)){
            ShowFrontCameraView();
        }

        if (Input.GetKeyDown(KeyCode.Keypad4)){
            ShowMouffleCameraView();
        }
    }

    public void ShowTpsCameraView() {
        MouffleCamera.enabled=false;
        CentralCamera.enabled = false;
        TpsCamera.enabled = true;
        FrontCamera.enabled = false;
    }
    public void ShowCentralCameraView() {
        MouffleCamera.enabled=false;       
        TpsCamera.enabled = false;
        CentralCamera.enabled = true;
        FrontCamera.enabled = false;
    }

    public void ShowFrontCameraView() {
        MouffleCamera.enabled=false;
        TpsCamera.enabled = false;
        CentralCamera.enabled = false;
        FrontCamera.enabled = true;
    }

    public void ShowMouffleCameraView() {
        TpsCamera.enabled = false;
        CentralCamera.enabled = false;
        FrontCamera.enabled = false;
        MouffleCamera.enabled=true;
    }
    
}
