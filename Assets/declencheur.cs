using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declencheur : MonoBehaviour
{
    public static int  count = 0;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(" Z,Q,S,D : le chariot\nles flèches pour les roues\nA,E : Mouffle\nR,T : extension des pates\nF,G : rotation des pates\nW,X: fleche\nCamera : Numpad[1,2,3,4]");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cube"))
        {
            count++;
            Debug.Log("Nombre de cubes dans la Trigger : " + count);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Cube"))
        {
            if (count > 0) 
            {
                count--;
            }
            Debug.Log("Nombre de cubes dans la Trigger : " + count);
        }
    }
}
