using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGrue : MonoBehaviour
{
    public float torque = 250f;
    public float forceChariot = 500f;
    public float forceMoufle = 500f;
    public ArticulationBody pivot;
    public ArticulationBody chariot;
    public ArticulationBody moufle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
        //commande pour la moufle
        if (Input.GetKey(KeyCode.N))
        {
        moufle.AddRelativeForce(transform.up * forceMoufle);}
        if (Input.GetKey(KeyCode.B))
        {
        moufle.AddRelativeForce(transform.up * -forceMoufle);}
    }
}
